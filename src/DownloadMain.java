import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DownloadMain {

	public static void main(String[] args) throws ExecutionException {

		ArrayList<String> l = new ArrayList<>();
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z1_29IX-1X_2017.pdf");
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z2_13X-15X_2017.pdf");
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z3_27X-29X_2017.pdf");
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z4_17X-19XI_2017.pdf");
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z5_1XII-3XII_2017.pdf");
		l.add("http://ii.up.krakow.pl/pdf/schedules/2017-18/niestacjonarne/II%20rok/INF%20II%20s3%20z6_15XII-17XII_2017.pdf");
		
		ArrayList<Future<DownloadInfo>> futures = new ArrayList<>();
		ExecutorService pool = Executors.newFixedThreadPool(3);
		for(String temp:l){
			if(temp != null)
			futures.add(pool.submit(new DownloadTask(temp)));
			else System.out.println("brak pliku");
		}
			pool.shutdown();
			try {
				if(pool.awaitTermination(15, TimeUnit.SECONDS)){
					for(Future<DownloadInfo> temp:futures)
					{
						System.out.println("Plik " + temp.get().getFileName()+ " o rozmiarze  " 
						+temp.get().getFileSize() + " za�adowany w " +temp.get().getLoadTime() 
						+ " milisekund przez w�tek "+temp.get().getThreadName());
						
						
					}
					
				}else
				{
					System.out.println("Pliki nie zosta�y pobrane w wyznaczonym czasie");
				}
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		
		
		
				
	}

}
