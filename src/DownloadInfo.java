
public class DownloadInfo {

	private long loadTime;
	private int fileSize;
	private String fileName,threadName;

	public String getThreadName() {
		return threadName;
	}

	public DownloadInfo(long loadTime, int fileSize, String fileName,String threadName) {
		super();
		this.loadTime = loadTime;
		this.fileSize = fileSize;
		this.fileName = fileName;
		this.threadName=threadName;
	}

	public long getLoadTime() {
		return loadTime;
	}

	public int getFileSize() {
		return fileSize;
	}

	public String getFileName() {
		return fileName;
	}
}
