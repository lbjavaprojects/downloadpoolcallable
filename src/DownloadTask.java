import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadTask implements Callable<DownloadInfo> {
	
	private String path;
	private String fileName;
	
	public DownloadTask(String path)
	{
		super();
		this.path = path;
		fileName = extractFileName();
	}
	@Override
	public DownloadInfo call() throws Exception {
		
		long timeStart=0, timeStop=0;
		int fileSize=0;
	
		
		timeStart=System.currentTimeMillis();
		URL url=new URL(path);
		try(InputStream is= url.openStream();
		FileOutputStream fos = new FileOutputStream(extractFileName())){
			int buffersize = 1024, c;
			byte [] buffer = new byte[buffersize];
			while((c=is.read(buffer,0,buffersize))>-1){
				fos.write(buffer, 0 , c);
				fileSize+=c;
			}
		timeStop = System.currentTimeMillis();
			
			
		}
		
		return new DownloadInfo(timeStop-timeStart, fileSize, fileName,Thread.currentThread().toString());
	}

	public String extractFileName(){
		return path.substring(path.lastIndexOf("/")+1);
	}
}
